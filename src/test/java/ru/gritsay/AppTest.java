package ru.gritsay;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Unit test for simple App.
 */
public class AppTest {

    @Test
    public void connect() {
        LikeServiceImpl likeService = new LikeServiceImpl();
        assertNotNull(likeService.getQueryManager());
        assertNotNull(likeService.getQueryManager().getClient());
    }

    @Test
    public void count1() {
        LikeService likeService = new LikeServiceImpl();
        likeService.like("p1");
        likeService.like("p2");
        likeService.like("p3");
        assertEquals(1, likeService.getLikes("p1"));
    }

    @Test
    public void count2() {
        LikeService likeService = new LikeServiceImpl();
        likeService.like("p1");
        likeService.like("p1");
        assertEquals(1, likeService.getLikes("p1"));
    }
}
