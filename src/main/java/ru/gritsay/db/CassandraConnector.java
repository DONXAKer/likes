package ru.gritsay.db;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.Session;

import java.io.Closeable;

public class CassandraConnector implements Closeable {
    public static final String KEYSPACE_MAIN = "likes";
    public static final String COLUMN_FAMILY_COUNT = "likecounts";
    public static final String COLUMN_FAMILY_PLAYER_LIKES = "likeforplayer";
    private Cluster cluster;
    private Session session;
    private String node = "localhost";
    private Integer port = 9042;

    void connect(final String node, final Integer port) {
        if (node != null && !"".equals(node)) {
            this.node = node;
        }
        if (port != null && port != 0) {
            this.port = port;
        }
        System.out.printf("Подключение к %s:%s\n", this.node, this.port);
        this.cluster = Cluster.builder().addContactPoint(this.node).withPort(this.port).build();
        final Metadata metadata = cluster.getMetadata();
        System.out.printf("Подключен: %s\n", metadata.getClusterName());
        session = cluster.connect();
    }

    Session getSession() {
        return this.session;
    }

    @Override
    public void close() {
        cluster.close();
    }
}