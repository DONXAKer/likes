package ru.gritsay.db;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;

import static ru.gritsay.db.CassandraConnector.*;

public class QueryManager {
    private final CassandraConnector client;

    public QueryManager() {
        this.client = new CassandraConnector();
        connect();
    }

    private void connect() {
        connect(null, null);
    }

    private void connect(String node, Integer port) {
        client.connect(node, port);
    }

    public long getCountLike(String id) {
        try {
            String query = String.format("SELECT counts FROM %s.%s WHERE playerid='%s';", KEYSPACE_MAIN, COLUMN_FAMILY_COUNT, id);
            return getRow(query).getLong(0);
        } catch (Exception e) {
            System.out.printf("Ошибка: %s", e.getMessage());
            return 0;
        }
    }

    private boolean existLike(String playerId, String playerFromId) {
        try {
            String query = String.format("SELECT COUNT(*) FROM %s.%s WHERE playerid='%s' AND playerfromid='%s';", KEYSPACE_MAIN, COLUMN_FAMILY_PLAYER_LIKES, playerFromId, playerId);
            return getRow(query).getLong(0) > 0;
        } catch (Exception e) {
            System.out.printf("Ошибка: %s", e.getMessage());
            return false;
        }
    }

    private Row getRow(String query) {
        System.out.printf("Запрос: %s\n", query);
        final ResultSet movieResults = client.getSession().execute(query);
        return movieResults.one();
    }

    public void updateLike(String playerId, String playerForLikeId) {
        boolean exist = existLike(playerForLikeId, playerId);
        String query;

        query = String.format("INSERT INTO %s.%s(playerid, playerfromid) values ('%s', '%s');", KEYSPACE_MAIN, COLUMN_FAMILY_PLAYER_LIKES, playerId, playerForLikeId);

        client.getSession().execute(query);
        if (!exist) {
            client.getSession().execute(String.format("UPDATE %s.%s SET counts=counts + 1 WHERE playerid='%s';", KEYSPACE_MAIN, COLUMN_FAMILY_COUNT, playerId));
        }
    }

    @Override
    protected void finalize() {
        client.close();
    }

    public CassandraConnector getClient() {
        return client;
    }
}
