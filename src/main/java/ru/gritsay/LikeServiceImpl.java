package ru.gritsay;

import ru.gritsay.db.QueryManager;

public class LikeServiceImpl implements LikeService {

    private final QueryManager queryManager = new QueryManager();

    @Override
    public void like(String playerId) {
        // Берется допустим из сессии
        String CURRENT_PLAYER_ID = "111";
        queryManager.updateLike(playerId, CURRENT_PLAYER_ID);
    }

    @Override
    public long getLikes(String playerId) {
        return queryManager.getCountLike(playerId);
    }

    QueryManager getQueryManager() {
        return queryManager;
    }
}
